let http = require("http");

const server = http
  .createServer((request, response) => {
    if (request.url === "/" && request.method === "GET") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.end("Welcome to Booking System");
    }

    if (request.url === "/profile" && request.method === "GET") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.end("Welcome to your profile!");
    }

    if (request.url === "/courses") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.end("Here are our courses available");
    }

    if (request.url === "/addCourse" && request.method === "POST") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.end("Add a course to our resources");
    }
  })
  .listen(4000);

console.log(`Server is running at localhost:4000`);
